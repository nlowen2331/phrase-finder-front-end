import React from 'react'
import '../css/DropScreen.css'

function DropScreen(props) {
    return (
        <div className='dropscreen-main'>
            <h1>Drag & Drop Your Text File</h1>
            <div {...props.rootProps()} className='dropzone'>
                <input {...props.inputProps()} />
                <p>Drop file here</p>
            </div>
            
        </div>
    )
}

export default DropScreen
