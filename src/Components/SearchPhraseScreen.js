import React, { useState } from 'react'
import '../css/SearchPhraseScreen.css'
import {MdCancel} from 'react-icons/md'
import { useTransition,animated } from 'react-spring'
import Loader from 'react-loader-spinner'

const ENDPOINT = 'https://tmg43z6zfe.execute-api.us-east-2.amazonaws.com/PhraseFinder'

const SearchPhraseScreen =(props)=> {

    const [input,setInput] = useState('')
    const [error,setError] = useState(`Enter a phrase in the searchbar, we'll search through the text for you`)
    const [hits,setHits] = useState([])
    const [hitCount,setHitCount] = useState(null)
    const [isLoading,toggleIsLoading] = useState(false)

    const GetMatches=async()=>{

        setError('')
        setHits([])
        setHitCount(null)
        toggleIsLoading(true)

        let SplitError=null

        try{
            var [phrase,limit] = SplitInput(input)
        }
        catch(err){
            SplitError=err
        }

        if(SplitError){
            console.log(SplitError)
            setHits([])
            toggleIsLoading(false)
            setError('Error occured, please try again!')
            setHitCount(null)
        }

        const ReqForm = new FormData()

        ReqForm.append('phrase',phrase)
        ReqForm.append('limit',limit ? Number(limit) : undefined)
        ReqForm.append('text',props.file[0])

        let Response = await fetch(ENDPOINT,{
            method: 'POST',
            body: ReqForm
        })

        switch(Response.status){
            case 200:
                let ResBody = await Response.json()
                console.log(ResBody)
                if(ResBody.snippets.length<1){
                    setError(`No results, try another phrase!`)
                    toggleIsLoading(false)
                    setHits([])
                    setHitCount(null)
                }
                else{
                    toggleIsLoading(false)
                    setHits(ResBody.snippets)
                    setHitCount(ResBody.hits)
                } 
                break;
            default:
                toggleIsLoading(false)
                setError(`Sorry, we couldn't understand, try again!`)
                setHits([])
                setHitCount(null)
                break;

        }

    }

    const transitions = useTransition(hits,hit=>hit.indexOf,{
        from: {opacity: 0,transform: 'translateY(-100%)',position: 'absolute'},
        enter: {opacity: 1,transform: 'translateY(0)',position: 'static'},
        leave: {opacity: 0,transform: 'translateY(-100%)',height: 0,padding: 0,fontSize: 0,margin:0,border: 'none'}
    })
    

    return (
        <div className='search-main'>
            <div className='loaded-file-container'>
                <div className='loaded-file' onClick={()=>props.dropFile()}>
                    {props.file[0].name}
                </div>
                <MdCancel className='cancel-icon' onClick={()=>props.dropFile()}/>
            </div>
            <div className='searchbar'>
                <input placeholder='tomato soup : 100' value={input} onChange={(e)=>setInput(e.target.value)}
                    onKeyDown={(e)=>{if(e.key==='Enter')GetMatches()}}/>
                <div onClick={()=>GetMatches()} className='search'>
                    <img src='./svgs/search.svg' width='100px' height='100px'/>
                </div>  
            </div>
            <div>
                {hitCount ? `${hitCount} hits` : ''}
            </div>
            <div className='results-container'>
            <Loader type='Puff' style={{display: isLoading ? '' : 'none',marginTop: '10px',alignSelf: 'center'}} color='#00b3b3'/>
                {hits.length===0 ?
                    <h2>
                        {error}
                    </h2>
                    :
                    transitions.map(({item,key,props})=>(
                        <animated.div style={props} key={key} className='result'>
                            <div>
                                {item.snippet}
                            </div>
                            <div className='result-index'>
                                {item.indexOf}
                            </div>
                        </animated.div>
                    ))
                }
            </div>
            
        </div>
    )
}

//Split the input into the phrase
const SplitInput = (input)=>{
    let array = input.split(':')
    return[...array]
}

export default SearchPhraseScreen
