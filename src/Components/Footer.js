import React from 'react'

function Footer() {
    return (
        <div className='footer'>
            <p>NRL Web Development & Solutions</p>
        </div>
    )
}

export default Footer
