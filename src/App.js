import React,{ useState } from "react";
import DropScreen from "./Components/DropScreen";
import SearchPhraseScreen from "./Components/SearchPhraseScreen";
import {useDropzone} from 'react-dropzone'
import './css/App.css'
import Footer from "./Components/Footer";

const App =()=>{

  const [file,setFile] = useState(null)

  const onDropAccepted=(file)=>setFile(file)

  const {getRootProps,getInputProps} = useDropzone({
      onDropAccepted,
      accept: 'text/plain',
      maxFiles: 1
  });

  return (
    <div className="App">
        {!file ?
              <DropScreen inputProps={getInputProps} rootProps={getRootProps}/>
            :
              <SearchPhraseScreen file={file} dropFile={()=>setFile(null)}/>
        }
        <Footer/>
    </div>
  );
}

export default App;
